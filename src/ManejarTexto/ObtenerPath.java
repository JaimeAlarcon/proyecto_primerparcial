/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejarTexto;

/**
 * 
 * @author Edison Barreiro
 */
public class ObtenerPath 
{
    /**
    * Variable que almacenará la dirección del proyecto
    */
    private static String path;
    
    /**
     * 
     * @param String nombreArchivo
     * @return String path
     */
    public static String obtenerPath(String nombreArchivo)
    {
        /**
         * Variable que almacenará la dirección local del proyecto
         */
        String directorio = System.getProperty("user.dir");
        
        /**
         * Variable que almacernará la jerarquización dentro del proyecto
         */
        
        String rutaProyecto = "\\src\\Archivos\\";
        
        String path = new String("");
        
        for (int i=0; i<(directorio+rutaProyecto+nombreArchivo).length();i++)
        {
            if ((directorio+rutaProyecto+nombreArchivo).charAt(i)=='\\')
            {
                path+="\\\\";
            }
            else
            {
                path+=(directorio+rutaProyecto+nombreArchivo).charAt(i);
            }
        }
        return path;
    }
}
