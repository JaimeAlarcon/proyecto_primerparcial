/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManejarTexto;

import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Edison Barreiro
 * 
 * Clase que sirve para manipulación de texto en un documento
 */
public class ManejoTexto 
{
    /**
     * Método estático que sirve para obtener usuario dentro de un documento
     * en este caso usuarios.txt
     * 
     * @param String path
     * @return String textoMostrar
     */
    public static String obtenerUsuarios(String path)
    {
        String texto = new String("");
        ArrayList<String> contenedor = new ArrayList<>();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        
        try
        {
            /**
             * Apertura del fichero y creacion de BufferedReader para poder leer
            */
            archivo = new File(path);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            
            while((linea=br.readLine())!=null)
            {   
                if (linea.length()!=0)
                    texto = texto.concat(linea).concat("\n");
            }
        }
        catch(Exception e)
        {
           e.printStackTrace();
        }finally
        {
            try{
                if (null!=fr)
                {
                    fr.close();
                }
            }catch (Exception e2)
            {
                e2.printStackTrace();
                }
            }
        /**
         * Variable que alamacenará el texto que retornará el método
         */
        String textoMostrar = new String("");
        for (int i=0; i<texto.length()-1;i++)
        {
            textoMostrar = textoMostrar.concat(Character.toString(texto.charAt(i)));
        }
        int contador = 0;
        for (int i=0; i<textoMostrar.length(); i++)
       {    
           String aux = "";
           if (textoMostrar.charAt(i)!='\\')
               aux = aux.concat(Character.toString(texto.charAt(i)));
           else 
               contenedor.add(aux);
               aux = "";
       }
       return textoMostrar;
    }
    
    /**
     * Método estático que srive para guardar datos en un documento,
     * en este caso usuarios.txt
     * 
     * @param String[] atributos 
     */
    public static void escribirArchivo(String[] atributos)
    {
        FileWriter fichero = null;
            PrintWriter pw = null;
            try
            {
                fichero = new FileWriter(ManejarTexto.ObtenerPath.obtenerPath("usuarios.txt"),true);
                pw = new PrintWriter(fichero);
            
                for (int i=0; i<5; i++)
                    switch(i)
                    {
                        case 0:
                            pw.print(atributos[0]+",");
                            break;
                        case 1:
                            pw.print(atributos[1]+",");
                            break;
                        case 2:
                            pw.print(atributos[2]+",");
                            break;
                        case 3:
                            pw.print(atributos[3]+",");
                            break;
                        case 4:
                            pw.println(atributos[4]);
                            break;
                    }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } finally
            {
                try 
                {
                    if (null!=fichero)
                        fichero.close();
                } catch (Exception e2)
                {
                e2.printStackTrace();
                }
            }
    }

    /**
     * 
     * @param String path
     * @return String textoMostrar
     */
    public static String obtenerPeliculas(String path)
    {
        String texto = new String("");
        ArrayList<String> contenedor = new ArrayList<>();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        
        try
        {
            /**
             * Apertura del fichero y creacion de BufferedReader para poder leer
             */
            archivo = new File(path);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            
            while((linea=br.readLine())!=null)
            {   if (linea.length()!=0)
                    texto = texto.concat(linea).concat("\n");
            }
        }
        catch(Exception e)
        {
           e.printStackTrace();
        }finally
        {
            try{
                if (null!=fr)
                {
                    fr.close();
                }
            }catch (Exception e2)
            {
                e2.printStackTrace();
                }
            }
        /**
         * Variable que alamacenará el texto que retornará el método
         */
        String textoMostrar = new String("");
       
        for (int i=0; i<texto.length()-1;i++)
        {
            
            textoMostrar = textoMostrar.concat(Character.toString(texto.charAt(i)));
        }
        int contador = 0;
        for (int i=0; i<textoMostrar.length(); i++)
        {    
           String aux = "";
           if (textoMostrar.charAt(i)!='\\')
               aux = aux.concat(Character.toString(texto.charAt(i)));
           else 
               contenedor.add(aux);
               aux = "";
           
       }
       return textoMostrar;
    }
    
    /**
     * 
     * @param String[] atributos 
     */
    public static void escribirArchivoCompra(String[] atributos)
    {
        FileWriter fichero = null;
            PrintWriter pw = null;
            try
            {
                fichero = new FileWriter(ManejarTexto.ObtenerPath.obtenerPath("compras.txt"),true);
                pw = new PrintWriter(fichero);
            
                for (int i=0; i<6; i++)
                    switch(i)
                    {
                        case 0:
                            pw.print(atributos[0]+",");
                            break;
                        case 1:
                            pw.print(atributos[1]+",");
                            break;
                        case 2:
                            pw.print(atributos[2]+",");
                            break;
                        case 3:
                            pw.print(atributos[3]+",");
                            break;
                        case 4:
                            pw.print(atributos[4]+",");
                            break;
                        case 5:
                            pw.println(atributos[5]);
                            break;      
                    }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } finally
            {
                try 
                {
                    if (null!=fichero)
                        fichero.close();
                } catch (Exception e2)
                {
                e2.printStackTrace();
                }
            }
    }
    
    public static void escribirPelicula(String[] atributos)
    {
        FileWriter fichero = null;
            PrintWriter pw = null;
            try
            {
                fichero = new FileWriter(ManejarTexto.ObtenerPath.obtenerPath("peliculas.txt"),true);
                pw = new PrintWriter(fichero);
            
                for (int i=0; i<11; i++)
                    switch(i)
                    {
                        case 0:
                            pw.print(atributos[0]+",");
                            break;
                        case 1:
                            pw.print(atributos[1]+",");
                            break;
                        case 2:
                            pw.print(atributos[2]+",");
                            break;
                        case 3:
                            pw.print(atributos[3]+",");
                            break;
                        case 4:
                            pw.print(atributos[4]+",");
                            break;
                        case 5:
                            pw.print(atributos[5]+",");
                            break;
                        case 6:
                            pw.print(atributos[6]+",");
                            break;
                        case 7:
                            pw.print(atributos[7]+",");
                            break;
                        case 8:
                            pw.print(atributos[8]+",");
                            break;
                        case 9:
                            pw.print(atributos[9]+",");
                            break;
                        case 10:
                            pw.println(atributos[10]);
                            break;      
                    }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } finally
            {
                try 
                {
                    if (null!=fichero)
                        fichero.close();
                } catch (Exception e2)
                {
                e2.printStackTrace();
                }
            }
    }
    
public static void escribirFunciones(String[] atributos)
    {
        FileWriter fichero = null;
            PrintWriter pw = null;
            try
            {
                fichero = new FileWriter(ManejarTexto.ObtenerPath.obtenerPath("funciones.txt"),true);
                pw = new PrintWriter(fichero);
            
                for (int i=0; i<6; i++)
                    switch(i)
                    {
                        case 0:
                            pw.print(atributos[0]+",");
                            break;
                        case 1:
                            pw.print(atributos[1]+",");
                            break;
                        case 2:
                            pw.print(atributos[2]+",");
                            break;
                        case 3:
                            pw.print(atributos[3]+",");
                            break;
                        case 4:
                            pw.print(atributos[4]+",");
                            break;
                        case 5:
                            pw.println(atributos[5]+",");
                            break;
                    }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } finally
            {
                try 
                {
                    if (null!=fichero)
                        fichero.close();
                } catch (Exception e2)
                {
                e2.printStackTrace();
                }
            }
    }    

public static String obtenerFunciones(String path)
    {
        String texto = new String("");
        ArrayList<String> contenedor = new ArrayList<>();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        
        try
        {
            /**
             * Apertura del fichero y creacion de BufferedReader para poder leer
            */
            archivo = new File(path);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            
            while((linea=br.readLine())!=null)
            {   if (linea.length()!=0)
                    texto = texto.concat(linea).concat("\n");
            }
        }
        catch(Exception e)
        {
           e.printStackTrace();
        }finally
        {
            try{
                if (null!=fr)
                {
                    fr.close();
                }
            }catch (Exception e2)
            {
                e2.printStackTrace();
                }
            }
        /**
         * Variable que alamacenará el texto que retornará el método
         */
        String textoMostrar = new String("");
        for (int i=0; i<texto.length()-1;i++)
        {
            textoMostrar = textoMostrar.concat(Character.toString(texto.charAt(i)));
        }
        int contador = 0;
        for (int i=0; i<textoMostrar.length(); i++)
       {    
           String aux = "";
           if (textoMostrar.charAt(i)!='\\')
               aux = aux.concat(Character.toString(texto.charAt(i)));
           else 
               contenedor.add(aux);
               aux = "";
       }
       return textoMostrar;
    }
}