/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal;

import java.util.Scanner;
import Validaciones.Validacion;

/**
 *
 * @author Jaime Alarcón
 */
public class Administrador extends Persona{
   //Arreglo de String en donde se guarda toda la información de la instancia
    //de la clase Administrador
    private static String[] info;          
    private String usuario;
    private String contraseña;
    /**
     * Constructor por defecto
     */
    public Administrador()
    {
        this("","",0,"","");
    }
    
    /**
     * Constructor con parametros
     * @param nombre
     * @param apellido
     * @param edad
     * @param usuario
     * @param contraseña 
     */
    public Administrador(String nombre, String apellido, int edad, String usuario, String contraseña)
    {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.usuario = usuario;
        this.contraseña = contraseña;
    }
    
    /**
     * Constructor copia
     * @param admin 
     */
    public Administrador(Administrador admin)
    {
        this.nombre = admin.nombre;
        this.apellido = admin.apellido;
        this.edad = admin.edad;
        this.usuario = admin.usuario;
        this.contraseña = admin.contraseña;
    }
    
    /**
     * Método estático que permite a la clase administrador los usuario que
      se guardarán en el documento usuario.txt
     */
    public static void crearUsuario()
    {
        info = new String[5];
        //bandera para validar que la primera letra sea mayuscula
        boolean primeraLetraEsMayus = false;
        //bandera para validarque la longitud sea la correcta
        boolean longitudCorrecta = false;
        
        Scanner sc = new Scanner(System.in);
        //controlador de menu de administrador
        String op = new String("");
        do
        {
            System.out.println();
            System.out.println("¿Qué perfil tendrá el nuevo usuario?");
            System.out.println("1. Cajero");
            System.out.println("2. Planificador");
            System.out.println("3. Cliente");
            System.out.println();
            System.out.print("Seleccione: ");
            op = sc.nextLine();
            
            //Asignacion de cargo al nuevo usuario
            if (op.equals("1"))
                //Cajero
                info[4] = "C"; 
            else if (op.equals("2"))
                //Planificador
                info[4] = "P";
            else if (op.equals("3"))
                //cliente
                info[4] = "c";            
            else if (!op.equals("1") && !op.equals("2") && !op.equals("3"))
                System.out.println("Opcion incorrecta");
        } while (!op.equals("1") && !op.equals("2") && !op.equals("3"));
        
        System.out.print("Ingrese el nombre: ");
        String n = sc.nextLine();
        info[0] = n;
        System.out.print("Ingrese el apellido: ");
        String a = sc.nextLine();
        info[1] = a;
        System.out.print("Ingrese el usuario: ");
        String us = sc.nextLine();
        Validaciones.Validacion.usuarioDisponible(us);
        info[2] = us;
        //Verificación de que el usuario no este ingresado
        
        System.out.print("Ingrese la contrasena: ");
        String psw = sc.nextLine();
        //Verificación de que la contrasena creada sea la correcta
        Validaciones.Validacion.validarContraseña(psw);
        info[3] = psw;
        System.out.println();
        //Preguntar al administrador si quiere guardar al nuevo usuario
        //en el sistema, en este caso el documento usuario.txt
        System.out.print("¿Desea guardar los cambios realizados? (S|N): ");
        String guardarNuevoUsuario = (sc.nextLine()).toUpperCase();
        if (guardarNuevoUsuario.equals("S"))        
        {
            //Guardar en uuario.txt el nuevo usuario
            ManejarTexto.ManejoTexto.escribirArchivo(info);
        }
        
        System.out.println();
        System.out.println("USUARIO CREADO EXITOSAMENTE");
    }
    
    /**
     * 
     * @return info
     */
    public String[] getInfo()
    {
        return info;
    }       
}
