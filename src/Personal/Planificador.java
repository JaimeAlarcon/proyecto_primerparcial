/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 */
package Personal;

import Pelicula.*;
import java.sql.Timestamp;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import Funcion.Funcion;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Jaime Alarcón
 */
public class Planificador extends Persona{
    private String usuario;
    private String contraseña;    
    private ArrayList<Pelicula> peliculas= new ArrayList<Pelicula>();
    public Planificador()
    {
        this("","",0,"","");
    }
    
    public Planificador(String nombre,String apellido,int edad,String usuario, String contraseña) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.usuario = usuario;
        this.contraseña = contraseña;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public static void registrarPelicula()
    {
        String opcion="";
        System.out.println("Registrando pelicula");
        System.out.println("Ingrese el tipo de Pelicula a Ingresar");
        System.out.println("Largometrajes (L),Cortometrajes(C),Documentales(D):");
        Scanner sc= new Scanner(System.in);
        opcion = sc.nextLine();
        String tipo="";
        String pelicula="";
        if(opcion.equals("L")){
            System.out.print("Ingrese Titulo:");
            String titulo=sc.nextLine();
            System.out.print("Ingresar Director: ");
            String director=sc.nextLine();
            System.out.print("Ingresar genero: ");
            String genero=sc.nextLine();
            System.out.print("Ingresar Duracion(minutos): ");
            String duracion=sc.nextLine();
            System.out.println("Ingrese Descripcion de actores(Separado por comas) ");
            String actores=sc.nextLine();
            System.out.println("Disponible en 3D?(S/N");
            String disponible= sc.nextLine();
            System.out.println("Tinen restricción?(S/N)");
            String restriccion=sc.nextLine();
            System.out.println("Trailer: ");
            String trailer=sc.nextLine();
            System.out.println("Ingrese Sinopsis:");
            String url=sc.nextLine();
            
            pelicula= String.join(",", titulo,genero,null,director,duracion,actores,disponible,restriccion,"L",trailer,url);
            String[] atributos = pelicula.split(",");
            ManejarTexto.ManejoTexto.escribirPelicula(atributos);
        }
        else if(opcion.equals("C")){
            System.out.print("Ingrese Titulo:");
            String titulo=sc.nextLine();
            System.out.print("Ingresar Director: ");
            String director=sc.nextLine();
            System.out.print("Ingresar genero: ");
            String genero=sc.nextLine();
            System.out.print("Ingresar Duracion(minutos): ");
            String duracion=sc.nextLine();
            System.out.print("Ingrese Descripcion de actores(Separado por comas) ");
            String actores=sc.nextLine();
            System.out.println("Disponible en 3D?(S/N");
            String disponible= sc.nextLine();
            System.out.println("Tinen restricción?(S/N)");
            String restriccion=sc.nextLine();
            System.out.print("Productor:");
            String productor = sc.nextLine();
            System.out.print("Es nacional o internacional:");
            String nacOint = sc.nextLine();
            pelicula= String.join(",", titulo,genero,null,director,duracion,actores,disponible,restriccion,"L",productor,nacOint);
            String[] atributos = pelicula.split(",");
            ManejarTexto.ManejoTexto.escribirPelicula(atributos);
        }
        else if(opcion.equals("D")){
            System.out.print("Ingrese Titulo:");
            String titulo=sc.nextLine();
            System.out.print("Ingresar Director: ");
            String director=sc.nextLine();
            System.out.print("Ingresar genero: ");
            String genero=sc.nextLine();
            System.out.print("Ingresar Duracion(minutos): ");
            String duracion=sc.nextLine();
            System.out.print("Ingrese Descripcion de actores(Separado por comas) ");
            String actores=sc.nextLine();
            System.out.println("Disponible en 3D?(S/N");
            String disponible= sc.nextLine();
            System.out.println("Tinen restricción?(S/N)");
            String restriccion=sc.nextLine();
            System.out.print("Tema:");
            String tema = sc.nextLine();
            System.out.print("Contiene testimonios reales:");
            String testimonios = sc.nextLine();
            pelicula= String.join(",", titulo,genero,null,director,duracion,actores,disponible,restriccion,"L",tema,testimonios);
            String[] atributos = pelicula.split(",");
            ManejarTexto.ManejoTexto.escribirPelicula(atributos);
        }
    }
    
     public static void registrarFunciones()
     {
         Funcion.registrarFuncion();
     }
     
      public static void consultarCartelera(){
        Menús.ConsultarCartelera.show();
        
    }
    public static Timestamp FechaHora(String fechaHoraS) throws ParseException{           
        SimpleDateFormat formatohorario = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Timestamp fechaHora_tiempo = new Timestamp(formatohorario.parse(fechaHoraS).getTime());
        Timestamp fecha = new Timestamp(formatohorario.parse(fechaHoraS).getTime());
        return fechaHora_tiempo;
    }
     public void consultarPorNombre(String nombre){
         Menús.ConsultarCartelera.show();
         
         

}

    
}
   