/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal;

/**
 *
 * @author Jaime Alarcón
 */
public class Cajero extends Persona 
{
    private String usuario;
    private String contraseña;
    
    /**
     * Constructor por defecto de clase Cajero
     */
    public Cajero()
    {
        this("","",0,"","");
    }

    /**
     * Constructor de clase Cajero
     * @param nombre
     * @param apellido
     * @param edad
     * @param usuario
     * @param contraseña 
     */
    public Cajero(String nombre,String apellido,int edad,String usuario, String contraseña) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.usuario = usuario;
        this.contraseña = contraseña;
    }
    
    /**
     * 
     * @param c 
     */
    public Cajero(Cajero c){
        this.nombre = c.nombre;
        this.apellido = c.apellido;
        this.edad = c.edad;
        this.usuario = c.usuario;
        this.contraseña = c.contraseña;
    }

    /**
     * 
     * @return String
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * 
     * @param usuario 
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * 
     * @return 
     */
    public String getContraseña() {
        return contraseña;
    }

    /**
     * 
     * @param contraseña 
     */
    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    /**
     * 
     */
    public static void registrarPago()
    {
        Menús.MenuGeneros.show();
        Pelicula.Pelicula.mostrarPorGenero(Menús.MenuGeneros.getOp());
        System.out.println();
        Pelicula.Pelicula.setHorarioPeliculaElejida();
        System.out.println();
        Menús.MenuInformacionUsuario.show();
        
        Color.Out.println(Color.Out.ROJO, "---------------------------------------");
        System.out.println();
        Color.Out.println(Color.Out.ROJO, "DETALLE COMPRA");
        System.out.println();
        Color.Out.println(Color.Out.ROJO, "---------------------------------------");
        System.out.println();
        Color.Out.print(Color.Out.AZUL, "Pelicula: ");
        Color.Out.println(Color.Out.PURPURA, ("\t")+Pelicula.Pelicula.getPeliculaElejida().getTitulo());
        System.out.println();
        Color.Out.print(Color.Out.AZUL, "Hora: ");
        Color.Out.println(Color.Out.PURPURA, ("\t\t")+Pelicula.Pelicula.getHorarioPeliculaElejida());
        System.out.println();
        Color.Out.print(Color.Out.AZUL, "Boletos: ");
        Color.Out.println(Color.Out.PURPURA, ("\t")+Menús.MenuInformacionUsuario.getCantidad());
        System.out.println();
        Color.Out.print(Color.Out.AZUL, "Total: ");
        Color.Out.println(Color.Out.PURPURA, ("\t\t")+Menús.MenuInformacionUsuario.getTotal());
        System.out.println();
        Color.Out.print(Color.Out.AZUL, "Sala: ");
        Color.Out.println(Color.Out.PURPURA, ("\t\t")+"1");
        System.out.println();
        Color.Out.print(Color.Out.AZUL, "Cliente: ");
        Color.Out.println(Color.Out.PURPURA, ("\t")+Menús.MenuInformacionUsuario.getCliente());
        Color.Out.println(Color.Out.PURPURA, "Gracias por su compra");
        System.out.println();
        Color.Out.println(Color.Out.AZUL, "---------------------------------------");
        System.out.println();
    }
}