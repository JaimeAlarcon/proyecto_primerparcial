/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Color;

/**
 *
 * @author Edison Barreiro
 * Clase que se encarga del manejo de color de la Interfaz 
 */
public class Color 
{
    public static final String ANSI_RESET = "\u001B[0m";
    
    public static final String ROJO = "\u001B[31m";
    
    public static final String AZUL = "\u001B[34m";
    
    public static final String PURPURA = "\u001B[35m";

    public static void println(String color, String txt)
    {
        System.out.println(color+txt);
    }
    /**
     * 
     * @param color
     * @param String txt
     * @return 
     */
    public static String getStringColor(String color, String txt)
    {
        return (color+txt);
    }
}
