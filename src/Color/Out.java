/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Color;

/**
 *
 * @author Edison Barreiro
 */
public class Out 
{
    /**
     * Constante que establece el texto a color negro(por defecto)
     */
    public static final String ANSI_RESET = "\u001B[0m";
    
    /**
     * Constatnte que establece el texto a color rojo
     */
    public static final String ROJO = "\u001B[31m";
    
    /**
     * Constante que establece el texto a color azul
     */
    public static final String AZUL = "\u001B[34m";
    
    /**
     * Constatnte que establece el texto a color purpura
     */
    public static final String PURPURA = "\u001B[35m";

    /**
     * Metodo que muestra texto con color y con un caracter "\n" al final
     * @param color
     * @param txt 
     */
    public static void println(String color, String txt)
    {
        System.out.println(color+txt);
    }
    
    /**
     * Metodo que muestra texto con color 
     * @param color
     * @param txt 
     */
    public static void print(String color, String txt)
    {
        System.out.print(color+txt);
    }
    
    /**
     * Metodo que retorna un texto con color
     * @param color
     * @param txt
     * @return String (color+txt)
     */
    public static String getStringColor(String color, String txt)
    {
        return (color+txt);
    }
}
