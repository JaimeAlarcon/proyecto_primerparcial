/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;

/**
 *
 * @author David Vega
 * Clase que se encarga de crear el menú para la cartelera.
 */
public class ConsultarCartelera 
{
    
    private static String op;
    /**
     * Menú de Cartelera.
     */
    public static void show()
    {
        do
        {
            op = new String("");
            System.out.println("------------------------------------");
            System.out.println("Consulta de Carteleras");
            System.out.println("------------------------------------");
            Scanner sc = new Scanner(System.in);
            System.out.println("1.  Por genero");
            System.out.println("2.  Por nombre de pelicula");
            System.out.println("3.  Por hora de inicio");
            System.out.print("Que desea hacer: ");
            op= sc.nextLine();
            if ((!op.equals("1") && !op.equals("2") && !op.equals("3")))
                System.out.println("Opcion incorrecta");
        } while(!op.equals("1") && !op.equals("2") && !op.equals("3"));
    }
    
    /**
     * getter de variable op
     * @return String op
     */
    public static String getOpcion()
    {
        return op;
    }
    
}
    

