/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;

/**
 *
 * @author Edison Barreiro
 */
public class MenuCajero 
{
    /**
     * Variable que almacenará la opción que el cajero va a seleccionar
     */
    private static String op;
    
    /**
     * Método estático que despliega el menú para el cajero
     */
    public static void show()
    {
        do 
        {
            op = new String("");
            Scanner sc = new Scanner(System.in);
            System.out.println("**************************************************");
            System.out.println();
            System.out.println("              BIENVENIDO CAJERO                   ");
            System.out.println();
            System.out.println("**************************************************");
            System.out.println();
            System.out.println();
            System.out.println("1.  Registrar Pago");
            System.out.println("2.  Salir");
            System.out.print("Ingrese opcion: ");
            op= sc.nextLine();
        } while(!op.equals("1") && !op.equals("2"));
    }
    
    /**
     * getter de variable op
     * @return String op
     */
    public static String getOpcion()
    {
        return op;
    }
}
