/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;
import Validaciones.Validacion;

/**
 *
 * @author Jaime Alarcón
 */
public class RegistroDatos {
    private static String usuario; 
    private static String contrasena; 
    private static boolean registrarUsuario; 
    private static boolean salir; 
    
    /**
     * Menu que se invoca cada vez que necesitemos pedir usuario y contraseña
     * le da valores a las variables usuario y contrasena
    */
    public static void menuDatos()
    {    
        Scanner sc = new Scanner(System.in);
        System.out.println("**************************************************");
        System.out.println();
        System.out.println("               BIENVENIDO AL SISTEMA              ");
        System.out.println();
        System.out.println("**************************************************");
        System.out.println();
        System.out.print("USUARIO: ");
        usuario = sc.nextLine();
        System.out.print("CONTRASEÑA: ");
        contrasena = sc.nextLine();    
    }   
    
    /**
     * Menu que se invoca para saber si el usuario quiere ingresar otro 
     * usuario o si quiere salir definitivamente del programa
     */
    public static void seguir_salir()
    {
        Scanner sc = new Scanner(System.in);
        String op = "";
        do 
        {
            System.out.println("1. Ingresar usuario");
            System.out.println("2. Salir");
            System.out.print("Que desea hacer??: ");
            op = sc.nextLine();
            if (op.equals("1"))
                registrarUsuario = true;
            else if (op.equals("2"))
                salir = true;
            else if (!op.equals("1") && !op.equals("2"))
            {
                System.out.println("Opcion incorrecta");
                System.out.println();
            }
        } while(!op.equals("1") && !op.equals("2"));
    }
    
    //getters de clase abstracta IngresoDeDatos
    public static boolean SeguirRegistrandoUsuario()
    {
        return registrarUsuario;
    }   
    
    public static boolean Salir()
    {
        return salir;
    }
    
    public static String getUsuario()
    {   
        return usuario;
    }
    
    public static String getContrasena()
    {
        return contrasena;
    }
}
