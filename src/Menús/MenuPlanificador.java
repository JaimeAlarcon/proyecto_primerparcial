/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;

/**
 *
 * @author David Vega
 */
public class MenuPlanificador 
{
    private static String op;
    
    public static void show()
    {
        do
        {
            op = new String("");
            Scanner sc = new Scanner(System.in);
            System.out.println("**************************************************");
            System.out.println();
            System.out.println("              BIENVENIDO PLANIFICADOR             ");
            System.out.println();
            System.out.println("**************************************************");
            System.out.println();
            System.out.println();
            System.out.println("1.  Registrar Películas");
            System.out.println("2.  Registrar Funciones");
            System.out.println("3.  Consultar Cartelera");
            System.out.println("4.  Salir");
            System.out.print("Ingrese opcion: ");
            op= sc.nextLine();
            
            if (!op.equals("1") && !op.equals("2") && !op.equals("3") && !op.equals("4"))
                System.out.println("Opcion incorrecta");
            
        } while(!op.equals("1") && !op.equals("2") && !op.equals("3") && !op.equals("4"));
    }
    
    /**
     * getter de variable op
     * @return String op
     */
    public static String getOpcion()
    {
        return op;
    }
    
}


