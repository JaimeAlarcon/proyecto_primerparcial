/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;
import Validaciones.Validacion;
import java.text.ParseException;

/**
 *
 * @author Jaime Alarcón
 */
public class MenuPrincipal 
{
    /**
     * Método estático que desplegará el menú principal del programa
     */
    public static void show() throws ParseException
    {
        /**
         * Variable local boolean que almacenará:
         * true, si se quiere seguir en el programa
         * false, si se quiere salir del programa
         */
        boolean salirPrograma = false;
        
        Scanner sc = new Scanner(System.in);
        do
        {
            Menús.RegistroDatos.menuDatos();
            System.out.println();
            
            Validacion.validacionUsuario(Menús.RegistroDatos.getUsuario(),Menús.RegistroDatos.getContrasena());
            System.out.println();
            
            /**
             * Si el usuario es un administrador inicia esta estructura de control
             */
            if (Validaciones.Validacion.getExisteUsuario()==true && Validaciones.Validacion.getOcupacion().equals("ADMINISTRADOR"))
            {
                Menús.MenuAdmi.show();
                if (Menús.MenuAdmi.getOpcion().equals("1"))
                    Personal.Administrador.crearUsuario();
                
                Menús.RegistroDatos.seguir_salir();
            }
            
            /**
             * Si el usuario es un cajero inicia esta estructura de control
             */
            else if (Validaciones.Validacion.getExisteUsuario()==true && Validaciones.Validacion.getOcupacion().equals("CAJERO"))
            {
                Menús.MenuCajero.show();
                if (Menús.MenuCajero.getOpcion().equals("1"))
                    Personal.Cajero.registrarPago(); 
                Menús.RegistroDatos.seguir_salir();
            }
            
            /**
             * Si el usuario es un planificador inicia esta estructura de control
             */
            else if (Validaciones.Validacion.getExisteUsuario()==true && Validaciones.Validacion.getOcupacion().equals("PLANIFICADOR"))
            {
                Menús.MenuPlanificador.show();
                if (Menús.MenuPlanificador.getOpcion().equals("1"))
                {
                    System.out.println("Registrar Pelicula");
                    Personal.Planificador.registrarPelicula();
                }   
                else if (Menús.MenuPlanificador.getOpcion().equals("2"))
                {
                    System.out.println("Registrar Funcion");
                    Funcion.Funcion.registrarFuncion();
                }
                else if (Menús.MenuPlanificador.getOpcion().equals("3"))
                {   Menús.ConsultarCartelera.show();
                    if (Menús.ConsultarCartelera.getOpcion().equals("1"))
                        Funcion.Funcion.consultarPorGenero();
                    else if(Menús.ConsultarCartelera.getOpcion().equals("2"))
                        Funcion.Funcion.consultarPorPelicula();
                    else if(Menús.ConsultarCartelera.getOpcion().equals("3"))
                        Funcion.Funcion.ConsultarPorHora();
                }
                
                Menús.RegistroDatos.seguir_salir();
            }
            
            else if (Validaciones.Validacion.getExisteUsuario()==false)
                Menús.RegistroDatos.seguir_salir();
            
        } while(Menús.RegistroDatos.Salir()==false);         
        
        /**
         * Si el usuario quiere salir del programa se inicia esta estructura de control
         */
        if (Menús.RegistroDatos.Salir()==true)
            System.out.println(Validaciones.Validacion.despedida());
    }
}
