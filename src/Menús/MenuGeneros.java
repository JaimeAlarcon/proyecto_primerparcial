/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;

/**
 *
 * @author Jaime Alarcon
 * 
 */
public class MenuGeneros 
{
    /**
     * Variable que almacenará la opción que el cajero va a seleccionar
     */
    private static String op;
    
    /**
     * Método estático que despliega el menú de género
     */
    public static void show()
    {
        do 
        {
            op = new String("");
            Scanner sc = new Scanner(System.in);
            System.out.println("GËNEROS DISPONIBLES");
            System.out.println("----------------------------");
            System.out.println("1. Ciencia Ficción ");
            System.out.println("2. Drama");
            System.out.println("3. Terror");
            System.out.println("4. Thriller");
            System.out.println("5. Acción");
            System.out.println("6. Comedia");
            System.out.println("7. Romántica");
            System.out.println("8. Aventura");
            System.out.println("9. Animadas");
            System.out.print("Ingrese opcion: ");
            op= sc.nextLine();
            
            if (!op.equals("1") && !op.equals("2") && !op.equals("3") && !op.equals("4") && !op.equals("5") 
                    && !op.equals("6") && !op.equals("7") && !op.equals("8") && !op.equals("9"))
                System.out.println("Opcion incorrecta");
        } while(!op.equals("1") && !op.equals("2") && !op.equals("3") && !op.equals("4") && !op.equals("5") 
                    && !op.equals("6") && !op.equals("7") && !op.equals("8") && !op.equals("9"));
    }

    /**
     * getter de variable op
     * @return String op
     */
    public static String getOp()
    {
        return op;
    }
}

