/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

/**
 *
 * @author Edison Barreiro
 */

import Compra.Codigo;
import java.util.Scanner;

public class MenuInformacionUsuario 
{
    /**
     * Variable estática que almacenará el nombre del cliente
     */
    private static String cliente;
    
    /**
     * Variable estática que almacenará la cédula
     */
    private static String cedula;
    
    /**
     * Variable estática que almacenará la cantidad de boletos
     */
    private static String cantidad;
    
    /**
     * Variable estática que almacenará si se quiere o no registra la compra en 
     * el archivo compras.txt
     */
    private static String registrarPago;
    
    /**
     * Constante que guarda el costo unitario de la película
     */
    public static final float COSTO = 5.0f;
    
    /**
     * Método estático que despliega las opciones para guardar la información
     * que el usuario quiere registrar
     */
    public static void show()
    {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Cliente: ");
        cliente = sc.nextLine();
        System.out.println();
        
        System.out.print("Cedula: ");
        cedula = sc.nextLine();
        System.out.println();
        
        System.out.print("Cantidad de boletos: ");
        cantidad = sc.nextLine();
        System.out.println();
        
        System.out.println("Valor a cancelar: "+(Integer.parseInt(cantidad)*COSTO));
        System.out.println();
        
        System.out.print("¿Desea registrar el pago? S|N : ");
        registrarPago = sc.nextLine().toUpperCase();
        System.out.println();
        
        Codigo.inicializarGeneradorDeCodigos();
        String codigo = Compra.Codigo.getCodigoCorrecto();
        
        if (registrarPago.equals("S"))
        {
            String[] atributosCompra = {codigo,Pelicula.Pelicula.getPeliculaElejida().getTitulo(),Pelicula.Pelicula.getHorarioPeliculaElejida(),null,cantidad,cliente};
            ManejarTexto.ManejoTexto.escribirArchivoCompra(atributosCompra);
        }
        
        System.out.println("PAGO REGISTRADO");
        System.out.println();
    }
    
    /**
     * Métodod estático que retorna la variable cantidad
     * @return String cantidad
     */
    public static String getCantidad()
    {
        return cantidad;
    }
    
    /**
     * Método estático que retorna la variable cliente
     * @return String cliente
     */
    public static String getCliente()
    {
        return cliente;
    }
    
    /**
     * Método estático que retorna el total a pagar
     * @return String (COSTO*cantidad)
     */
    public static String getTotal()
    {
        return (Float.toString(Integer.parseInt(cantidad)*COSTO));
    }
}
