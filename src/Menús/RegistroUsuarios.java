/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menús;

import java.util.Scanner;

/**
 *
 * @author Jaime Alarcón
 */
public class RegistroUsuarios {
    private static boolean registrarUser;
    private static boolean salir;
    
    /**
     * Métdodo estático que despliega el menú
     */
    public static void menuRegUsuarios()
    {
        String op = new String("");
        do 
        {
        Scanner sc = new Scanner(System.in);        
        System.out.println("1. Registrar usuario: ");
        System.out.println("2. Salir");
        op = sc.nextLine();
        
        if (op.equals("1"))
            registrarUser = true;
        else if (op.equals("2"))
            salir = true;
        else if (!op.equals("1") && !op.equals("2"))
                System.out.println("Opcion incorrecta");
                System.out.println();
        }   while (op.equals("1") || op.equals("2"));
    }
    
    
   
    
    public static boolean SeguirRegistrandoUsuario()
    {
        return registrarUser;
    }   
    
    public static boolean Salir()
    {
        return salir;
    }
}
