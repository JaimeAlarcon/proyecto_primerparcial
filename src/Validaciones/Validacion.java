/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validaciones;

import ManejarTexto.ManejoTexto;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Jaime Alarcon
 * @author Edison Barreiro
 * @author David Vega
 * 
 * 
 * Clase abstracta en la que se guarda todo tipo de operaciones, validaciones
 * entre usuarios y sus diferentes aspectos
 */
public abstract class Validacion
{
    //variable que guarda la ocupacion del usuario que se analizará
    private static String ocupacion;
    
    //Variable que guarda dirección de archivo usuarios.txt
    private static String path;
    
    //Variable para saber si el usuario que se está analizando se encuentra en usuarios.txt
    private static boolean existeUsuario;
    
    //Variable para saber si existe administrador, siempre será true ya que el administrador estará desde un principio en usuarios.txt
    private static boolean existeAdministrador; 
    
    //Variable para validar si la contrasena ingresada es correcta o no, se usa en el metodo estatico validacionUsuario
    private static boolean contrasenaIncorrecta;
    
    //Variable para validar que la contrasena que se creará para un nuevo usuario tenaga el formato correcto, se usa en el método estático validarContraseña
    private static boolean contrasenaCorrecta;
    
    //Constante para agradecerle al usuario el uso del programa
    private static final String DESPEDIDA = "Gracias por usar este cine :*";
    
    //Constatne para verificar que primera letra sea mayuscula
    private static final char[] ABECEDARIOMAYUSCULA = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        
    //ArrayList que sirve para saber que usuarios ya no están disponibles
    private static ArrayList<String> usuariosNoDisponible;
    
    //Variable para saber si esta disponible el usuario elejido por el administrador para nuevo personal o cliente
    private static boolean estaUsuarioDisponible;
    
    private static String pswFinal;
    /**
     * Método estático que indica si existe o no el usuario ingresado por el usuario
     * 
     * @param us
     * @param cont 
     * 
     */
    public static void validacionUsuario(String us, String cont)
    {
        contrasenaIncorrecta = false;
        ocupacion = new String("");
        path = ManejarTexto.ObtenerPath.obtenerPath("usuarios.txt");
        String usuariosLista = ManejoTexto.obtenerUsuarios(path);
        String[] usuariosSinBackslach = usuariosLista.split("\n");
        existeUsuario = false;
        existeAdministrador = false;
        String usuario = new String("");
        String contrasena = new String("");
        String cargo = new String("");
        for (String a:usuariosSinBackslach)
        {
            usuario = ((Arrays.asList((a.split((","))))).get(2));
            contrasena = ((Arrays.asList((a.split((","))))).get(3));
            cargo = ((Arrays.asList((a.split((","))))).get(4));
            if (usuario.equals(us))
            {
                existeUsuario = true;
                if (contrasena.equals(cont))  
                    
                    {
                    if (cargo.equals("A"))
                    {
                        existeAdministrador = true;
                        System.out.println("\t\t"+usuario+" es el ADMINISTRADOR");
                        ocupacion = "ADMINISTRADOR";
                    }
                    else if (cargo.equals("C"))
                    {
                        System.out.println("\t\t"+usuario+" es un CAJERO");
                        ocupacion = "CAJERO";
                    }
                    else if (cargo.equals("P"))
                    {
                        System.out.println("\t\t"+usuario+" es un PLANIFICADOR");
                        ocupacion = "PLANIFICADOR";
                    }
                    }
                else
                {
                    ocupacion = "NINGUNA";
                    existeUsuario = true;
                    System.out.println("Contrasena incorrecta");
                    contrasenaIncorrecta = true;
                }
            }         
        }
        if (existeUsuario==false)
            System.out.println("Usuario y contrasena incorrectos");      
    }
    
    /**
     * Método estático que sirve para saber si el usuario ingreso una contrasena correcta
     * 
     * @param psw 
     */
    public static void validarContraseña(String psw)
    {
        contrasenaCorrecta = false;
        Scanner sc = new Scanner(System.in);
        boolean primeraLetraEsMayus = false;
        boolean longitudCorrecta = false;
        
        for (char a:ABECEDARIOMAYUSCULA)
        {
            if (a==psw.charAt(0))
            {
                primeraLetraEsMayus = true;
            }
        }
        if (psw.length()>=8 && psw.length()<=12)
        {
            longitudCorrecta = true;
        }
        
        if (primeraLetraEsMayus==true && longitudCorrecta==true)
            pswFinal = psw;
        
        while (primeraLetraEsMayus==false || longitudCorrecta==false)
        {
            System.out.println();
            System.out.println("La contrasena ingresada es incorrecta");
            System.out.println("La contrasena debe tener el siguiente formato");
            System.out.println("1. Debe empezar con una letra mayúscula");
            System.out.println("2. Debe tener una longitud de 8 a 12 letras");
            System.out.print("Ingrese contrasena: ");
            psw = sc.nextLine();
            
            for (char a:ABECEDARIOMAYUSCULA)
            {
                if (a==psw.charAt(0))
                {
                    primeraLetraEsMayus = true;
                }
            }
            if (psw.length()>=8 && psw.length()<=12)
            {
                longitudCorrecta = true;
            }
        }
        
        if (primeraLetraEsMayus==true && longitudCorrecta==true)
            contrasenaCorrecta = true;
            pswFinal = psw;
    }
    
    /**
     * Método para validar si un nombre de usuario está disponible 
     * 
     * @return 
     */
    public static void usuarioDisponible(String us)
    {
        do
        {
            Scanner sc = new Scanner(System.in);
            estaUsuarioDisponible = true;
            usuariosNoDisponible = new ArrayList<>();
            path = ManejarTexto.ObtenerPath.obtenerPath("usuarios.txt");
            String usuariosLista = ManejoTexto.obtenerUsuarios(path);
            String[] usuariosSinBackslach = usuariosLista.split("\n");
            String usuario = new String("");
            for (String a:usuariosSinBackslach)
            {
                usuariosNoDisponible.add(((Arrays.asList((a.split((","))))).get(2)));
            }
            for (String a:usuariosNoDisponible)
            {
                if (a.equals(us))
                    estaUsuarioDisponible = false;
            }
            if (estaUsuarioDisponible==false)
            {
                System.out.println("Usuario ya en uso");
                System.out.print("Por favor ingrese otro usuario: ");
                String op = sc.nextLine();      
        }
        }while (estaUsuarioDisponible==false);
        
    }
    
    //getters de atributos de la clase Validaciones
    
    public static boolean getExisteUsuario()
    {
        return existeUsuario;
    }
    
    public static boolean getExisteAdiministrador()
    {        
        return existeAdministrador;
    }
    
    public static String getOcupacion()
    {
        return ocupacion;
    }
    
    public static String getPath()
    {
        return path;
    }
    
    public static boolean contrasenaIncorrecta()
    {
        return contrasenaIncorrecta;
    }
    
    public static String despedida()
    {
        return DESPEDIDA;
    }
    
    public static String getPswFinal()
    {
        return pswFinal;
    }
}

