/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcion;

import Compra.Codigo;
import Pelicula.Pelicula;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import Personal.Planificador;
import java.text.ParseException;
/**
 *
 * @author Jaime Alarcón
 */
public class Funcion{
    private String pelicula;
    private static String fechaHora;
    private String horaInicio;
    private String sala;
    private String codigo;
    private String disponible;
    private static ArrayList<Funcion> funciones;
    private static String horas [];

    public Funcion()
    {
        this("",null,"","","","");
    }
    
    public Funcion(String pelicula, String fechaHora, String horaInicio, String sala, String codigo, String disponible)
    {
        this.pelicula = pelicula;
        this.fechaHora = fechaHora;
        this.horaInicio = horaInicio;
        this.sala = sala;
        this.codigo = codigo;
        this.disponible = disponible;
    }
    
    public Funcion(String pelicula,String FechaHora, String sala) {
        this.pelicula = pelicula;
        this.fechaHora = fechaHora;
        this.sala = sala;
    }

    public String getPelicula() {
        return pelicula;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Timestamp FechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
     private int obtieneCodigo(){
        Random r = new Random();
        return r.nextInt(5600);
    }

     public static void registrarFuncion()
     {
         Scanner sc = new Scanner(System.in);
         System.out.print("Ingrese película: ");
         String pelicula = sc.nextLine();
         System.out.print("Ingrese fecha de la función: ");
         String fecha = sc.nextLine();
         System.out.print("Ingrese hora de inicio:");
         String horaInicio = sc.nextLine();
         System.out.print("Sala: ");
         String sala = sc.nextLine();
         
         Codigo.inicializarGeneradorDeCodigos();
         String codigo = Codigo.getCodigoCorrecto();
         
         String disponible = "30";
         
         String[] atributos = String.join(",", pelicula,fecha,horaInicio,sala,codigo,disponible).split(",");
         ManejarTexto.ManejoTexto.escribirFunciones(atributos);
     }
     
     public static void listarFunciones(String funcionesString)
     {
         funciones = new ArrayList<>();
         for (String funcion:funcionesString.split("\n"))
         {
             String pelicula = funcion.split(",")[0];
             String fecha = funcion.split(",")[1];
             String horaInicio = funcion.split(",")[2];
             String sala = funcion.split(",")[3];
             String codigo = funcion.split(",")[4];
             String disponible = funcion.split(",")[5];
             
             Funcion f = new Funcion(pelicula,fecha,horaInicio,sala,codigo,disponible);
             funciones.add(f);
         }
    }
     
    public static ArrayList<Funcion> mostrarFunciones()
    {
        return funciones;
    }
     
    public static void consultarPorGenero()
    {
        funciones= new ArrayList<>();
        if (Funcion.mostrarFunciones()==null)
            Funcion.listarFunciones(ManejarTexto.ManejoTexto.obtenerFunciones(ManejarTexto.ObtenerPath.obtenerPath("funciones.txt")));
        
        ArrayList<Funcion> funciones = Funcion.mostrarFunciones();
        ArrayList<ArrayList<String>> generos = new ArrayList<>();
        Pelicula.listarPeliculas();
        ArrayList<String> auxiliar = new ArrayList<>();
        int contador = 1;
        
        for (Funcion f:funciones)
        {
            //System.out.println("****");
            //System.out.println(f.pelicula);
            for (Pelicula p:Pelicula.mostrarPeliculas())
            {
                if (p.getTitulo().equals(f.pelicula))
                    //System.out.println(p);{
                    if (generos.contains(p.getGenero())==false)
                    {
                        auxiliar = new ArrayList<>();
                        auxiliar.add(Integer.toString(contador));
                        auxiliar.add(p.getGenero());
                        generos.add(auxiliar);
                        contador+=1;
                    }
            }
        }
        //System.out.println(generos);
        Scanner sc = new Scanner(System.in);
        String op = "";
        
        for (ArrayList<String> g:generos)
        {
            System.out.println(g.get(0)+". "+g.get(1));
            
        }
        
        System.out.print("Que género desea elejir: ");
        op = sc.nextLine();
        String opString = "";
        for (ArrayList<String> g:generos)
        {
            if (op.equals(g.get(0)))
            {
                opString = g.get(1);
            }
        }
        System.out.println(opString);
        
        ArrayList<Pelicula> compararPel = new ArrayList<>();
        
        for (Pelicula p:Pelicula.mostrarPeliculas())
        {
            if (p.getGenero().equals(opString))
            {
                compararPel.add(p);
            }
        }
        
        for (Pelicula p:compararPel)
        {
            for (Funcion f:funciones)
            {
                if (p.getTitulo().equals(f.pelicula))
                    
                    System.out.println("************\n"+f);
            }
        }
    }
    
    public static void consultarPorPelicula() throws ParseException
    {
        Scanner sc = new Scanner(System.in);
        String pelicula = "";
        
        
        System.out.print("Ingrese el nombre de la película (al menos 6 caracteres): ");
        pelicula = sc.nextLine();
        while (pelicula.length()<6)
        {
            System.out.println("**Ingrese al menos 6 caracteres para realizar la búsqueda");
            System.out.print("ngrese el nombre de la película (al menos 6 caracteres): ");
            pelicula = sc.nextLine();
        } 
        //descomentar lo de arriba y la linea de abajo
        
        
        if (funciones==null)
            Funcion.listarFunciones(ManejarTexto.ManejoTexto.obtenerFunciones(ManejarTexto.ObtenerPath.obtenerPath("funciones.txt")));
        //System.out.println(pelicula);
        //System.out.println(funciones);
        
        int len = pelicula.length();
        //System.out.println("Longitud de pelicula:"+pelicula.length());
        int contador = 1;
        ArrayList<Funcion> ordenadas= new ArrayList<Funcion>();
        ArrayList<Funcion> aOrdenar= new ArrayList<Funcion>();
        for (Funcion f:funciones)
        {
            //System.out.println(f);
            //System.out.println("**********peliculas de la funcion:*************");
            //System.out.println(f.pelicula);
            if (len<=f.pelicula.length())
            {
                /*
                System.out.println(f.pelicula.substring(0,len));
                System.out.println(pelicula);
                System.out.println((f.pelicula.substring(0,len)).toLowerCase().equals(pelicula.toLowerCase()));
                */ //borrar
                if ((f.pelicula.substring(0,len)).toLowerCase().equals(pelicula.toLowerCase()))
                {
                    aOrdenar.add(f);
                }
            }
            else 
            {
                /*
                System.out.println(f.pelicula);
                System.out.println(pelicula);
                System.out.println(f.pelicula.toLowerCase().equals(pelicula.toLowerCase()));
                */ //borrar
                if (f.pelicula.toLowerCase().equals(pelicula.toLowerCase()))
                {
                    aOrdenar.add(f);
                }
            }
            //System.out.println(f.pelicula.substring(0,len));
            
            //if (f.pelicula.substring(0,len).equals(pelicula))
            /*{
                //System.out.println(contador+". "+f.pelicula);
                //contador+=1;
                aOrdenar.add(f);
            }
            
            */
            //listat ordenada System.out.println(aOrdenar);
            
            
            for(Funcion s: aOrdenar ){
                int indice=0;
                indice=aOrdenar.indexOf(s);
                if(indice<=aOrdenar.size()&& indice>1){
			indice++;}
            
                if(s.pelicula.compareToIgnoreCase(aOrdenar.get(indice).pelicula)==-1){
                       ordenadas.add(s);
                    }
     
               else if(s.pelicula.compareToIgnoreCase(aOrdenar.get(indice).pelicula)==0){
                        Timestamp fecha= Planificador.FechaHora(s.fechaHora);
                        Timestamp fecha2 = Planificador.FechaHora(aOrdenar.get(indice).getFechaHora());
                        //Linea que da error
                        int valor= fecha.compareTo(fecha2);
                        if(valor==-1){
                            ordenadas.add(s);
                        }
                    ordenadas.add(s);
                   }
               else{
                   ordenadas.add(s);
               }
            }
            
            //System.out.println(ordenadas);
        }
            
            
        
        for (Funcion fun : ordenadas){
            System.out.println(contador+""+fun.pelicula);
            contador+=1;
      
        }
    }
    
    public static void ConsultarPorHora() throws ParseException{
        ArrayList<Funcion> Ordenada= new ArrayList<Funcion>();
        ArrayList<Funcion> Aordenar= new ArrayList<Funcion>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la hora a buscar: ");
        String hora = "";
        hora=sc.nextLine();
        for(Funcion f:funciones){
            horas = new String [2];
            horas=  fechaHora.split(" ");
            if(horas[1].equals(hora)){
                Aordenar.add(f);
            }
        }
        String genero = null;
        String genero2=null;
        for(Funcion f1:Aordenar){
            int indice=0;
            if(indice<=Aordenar.size()){
                indice++;
            }
            indice=Aordenar.indexOf(f1);
            Timestamp fechahora= Personal.Planificador.FechaHora(fechaHora);
            Timestamp fechahora2=Personal.Planificador.FechaHora(Aordenar.get(indice).getFechaHora());
            if(fechahora.getTime()<fechahora2.getTime()){
                Ordenada.add(f1);
                
            }
            else if(fechahora.compareTo(fechahora2)==0){
                if (Pelicula.mostrarPeliculas()==null)
                    Pelicula.listarPeliculas();
                for (Pelicula p:Pelicula.mostrarPeliculas())
                {
                    if (p.getTitulo().equals(f1.getPelicula()))
                        genero = p.getGenero();
                    if(p.getTitulo().equals(Aordenar.get(indice).getPelicula())){
                        genero2=p.getGenero();
                    }
                    if(genero.compareToIgnoreCase(genero2)==-1 ){
                        Ordenada.add(f1);
                    }
                }
                
            
        }
            
        }
        
        
        
        
        
    }
    
    @Override
    public String toString()
    {
        return  ("Pelicula: \t"+pelicula+"\n"+
                 "Feha: \t\t"+fechaHora+"\n"+
                 "Hora de inicio: "+horaInicio+"\n"+
                 "Sala: \t\t"+sala+"\n"+
                 "Codigo: \t"+codigo+"\n"+
                 "Disponible: \t"+disponible+"\n");
    }

   
}

 

 