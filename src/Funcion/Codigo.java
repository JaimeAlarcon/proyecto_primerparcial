/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcion;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Edison Barreiro
 */
public class Codigo 
{
    /**
     * Variable estática que almacenará el código que se generará al realizar una compra
     */
    private static String codigo;
    
    /**
     * ArrayList de objetos tipo String
     * Almacenará los códigos generados en las compras para evitar que los mismos se repitan
     */
    private static ArrayList<String> contenedorDeCodigos;
    
    /**
     * Método estático que retorna el codigo único´que se asignara a cada compra
     * @return String codigo
     */
    public static String getCodigoCorrecto()
    {
        String evaluar = "";
        codigo = "";
        
        do
        {
            evaluar = Codigo.generarCodigo();
            Codigo.verificarCodigo(evaluar);
        } while (Codigo.verificarCodigo(evaluar)==false);     
        
        codigo = evaluar;
        
        return codigo;
    }
    
    /**
     * Método estático que inicializa el ArrayList donde se almacenarán los códigos
     */
    public static void inicializarGeneradorDeCodigos()
    {
        contenedorDeCodigos = new ArrayList<>();
    }
    
    /**
     * Método estático que retorna el ArrayList de códigos
     * @return ArrayList<String> contenedorDeCodigos
     */
    public static ArrayList<String> getCodigos()
    {
        return contenedorDeCodigos;
    }
    
    /**
     * Método estático que verifica que el cógido ingresado no sea repetido
     * @param codigo
     * @return boolean esRepetido
     */
    public static boolean verificarCodigo(String codigo)
    {
        Random al = new Random();
        
        /**
         * Variable que almacenará:
         * true si el codigo es repetido
         * false si el codigo no es repetido
         */
        boolean esRepetido = false;

        if (contenedorDeCodigos.size()==0)
        {
            contenedorDeCodigos.add(codigo);
        }
        else
        {
            for (String cod:contenedorDeCodigos)
            {
                if (cod.equals(codigo))
                {

                    esRepetido = true;    
                }
            }
            if (esRepetido==false)
            {
                contenedorDeCodigos.add(codigo);
            }
        }
    return esRepetido;
    }
    
    /**
     * Método estático que genera el código que se asignará en las compras 
     * @return String codigo
     */
    public static String generarCodigo()
    {
        Random al = new Random();
        String codigo = "";
        int codigoInt = 0;
        do
        {
            int a = al.nextInt(10);
            int b = al.nextInt(10);
            int c = al.nextInt(10);
            int d = al.nextInt(10);
            codigo = Integer.toString(a)+Integer.toString(b)+Integer.toString(c)+Integer.toString(d);
            if (codigo.equals("0001"))
                codigoInt = 1;
            else if (codigo.equals("0010"))
                codigoInt = 10;
            else if (codigo.equals("0100"))
                codigoInt = 100;
            else
                codigoInt = Integer.parseInt(codigo);
            
        } while(codigoInt<1 && codigoInt>=5600);
        return codigo;
    }
}