/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;

import Personal.Persona;

/**
 *
 * @author Edison Barreiro
 * Clase que define Atributos y comportamientos de Un objeto cortometraje que hereda de Pelicula e implementa Reproducible 
 */
public class Cortometraje extends Pelicula implements Reproducible
{
    private Persona productor;
    private boolean nacionalOinternacional;
    /**
     * constructor 
     */
    public Cortometraje()
    {
        this(null,false);
    }
    /**
     * Constructor Sobrecargado
     * @param productor
     * @param nacionalOinternacional 
     */
    public Cortometraje(Persona productor, boolean nacionalOinternacional)
    {
        this.productor = productor;
        this.nacionalOinternacional = nacionalOinternacional;
    }
    /**
     * Setter de Productor
     * @param productor 
     */
    public void setProductor(Persona productor) {
        this.productor = productor;
    }
    /**
     * Setter de NacionalOInternacional
     * @param nacionalOinternacional 
     */
    public void setNacionalOinternacional(boolean nacionalOinternacional) {
        this.nacionalOinternacional = nacionalOinternacional;
    }
    /**
     * getter de Productor (Persona)
     * @return 
     */
    public Persona getProductor() {
        return productor;
    }
    /**
     * Setter de NacionalOInternacional
     * @return nacionalOinternacional
     */
    public boolean isNacionalOinternacional() {
        return nacionalOinternacional;
    }
    /**
     * Sobrescritura del metodo reproducir de la Interfaz reproducible
     * @return mensaje reproduciendo
     */
        @Override
    public String reproducir()
    {
        return "Reproduciendo";
    }
    /**
     * Sobrescritura del metodo Pausar de la Interfaz reproducible
     * @return 
     */
    @Override
    public String pausar()
    {
        return "Pausado";
    }
}
