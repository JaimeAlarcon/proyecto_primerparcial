/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;

import ManejarTexto.ManejoTexto;
import ManejarTexto.ObtenerPath;

/**
 *Definicion de la clase horario 
 * @author Edison Barreiro
 */
public class Horario 
{
    /**
     * Metodo que retorna un array de Horarios por pelicula
     * @param pelicula
     * @return 
     */
    public static String[] getHorario(Pelicula pelicula)
    {
        //System.out.println(pelicula);
        //System.out.print(Pelicula.mostrarPeliculas());
        if (pelicula.getHorario().equals("null"))
            {
                //System.out.println();
                //System.out.println("********************");
                //System.out.println("No hay horario disponible");
                return null;
            }
            else
            {
                //System.out.println();
                //System.out.println("********************");
                //System.out.println(pelicula.getTitulo());
                String h = (pelicula.getHorario().substring(1,pelicula.getHorario().length()-1));
                return (h.split(" ")[1]).split("-");    
            }
        }
    /**
     * Metodo que reorna las fechas por pelicula
     * @param pelicula
     * @return 
     */
    public static String getFecha(Pelicula pelicula)
    {
        //System.out.print(Pelicula.mostrarPeliculas());
        if (pelicula.getHorario().equals("null"))
            {
                //System.out.println();
                //System.out.println("********************");
                //System.out.println("No hay horario disponible");
                return null;
            }
            else
            {
                //System.out.println();
                //System.out.println("********************");
                //System.out.println(pelicula.getTitulo());
                String h = (pelicula.getHorario().substring(1,pelicula.getHorario().length()-1));
                return (h.split(" ")[0]);    
            }
    }
}
