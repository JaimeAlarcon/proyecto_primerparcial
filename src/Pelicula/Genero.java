/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;

/**
 *
 * @author Edison Barreiro 
 * Definicion de la clase Enum Genero que tendrá valores fijos ya definidos
 */


import java.util.ArrayList;

public enum Genero 
{   
    SCYFY("Ciencia Ficcion",new ArrayList<>()),
    DRAMA("Drama",new ArrayList<>()),
    TERROR("Terror",new ArrayList<>()),
    Thriller("Thriller",new ArrayList<>()),
    ACCION("Acción",new ArrayList<>()),
    COMEDIA("Comedia",new ArrayList<>()),
    ROMANTICA("Romántica",new ArrayList<>()),
    AVENTURA("Aventura",new ArrayList<>()),
    ANIMADAS("Animadas",new ArrayList<>());
    
    private String genero;
    private ArrayList<Pelicula> peliculas;
    /**
     * Constructor de Genero
     * @param genero
     * @param peliculas 
     */
    private Genero(String genero, ArrayList<Pelicula> peliculas)
    {
        this.genero = genero;
        this.peliculas = peliculas;
    }
}
