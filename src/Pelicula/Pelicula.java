/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;

/**
 *Clase que define comportamoentos Y atributos de una Pelicula , la cual será laplantilla principal para todos quienes hereden de ella
 * @author Jaime Alarcon
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Pelicula 
{
    /**
     * Variable que almacenará el título de película
     */
    protected String titulo;
    
    /**
     * Variable que almacenará el género de la película
     */
    protected String genero;
    
    /**
     * Variable que almacenará el horario de la película
     */
    protected String horario;
    
    /**
     * Variable que almacenará el director de película
     */
    protected String director;
    
    /**
     * Variable que almacenará la duración de película
     */
    protected float duracion;
    
    /**
     * Variable que almacenará la descripción de actores de la película
     */
    protected String descripcionActores;
    
    /**
     * Variable que almacenará si la película es o no 3d
     */
    protected String es3d;
    
    /**
     * Variable que almacenará si la película es apta para menores de edad
     */
    protected String aptaParaMenoresDeEdad;
    
    /**
     * Variable estática de tipo Película que al,acenará le película que el usuario desea ver
     */
    protected static Pelicula peliculaElejida;

    /**
     * Declaración de variable de tipo ArrayListz<Pelicula> que almacenará las películas que el cine 
     * tiene disponible. Se las obtiene del archivo peliculas.txt
     */
    protected static ArrayList<Pelicula> peliculas;
    
    /**
     * Constructor por defecto de la clase Pelicula
     */
    
    protected static String horarioElejido;
    
    public Pelicula()
    {
        this("","","","",0.0f,"","","");
    }
    
    /**
     * Constructor de clase Pelicula
     * @param  titulo
     * @param genero
     * @param horario
     * @param director
     * @param duracion
     * @param descripcionActores
     * @param es3d
     * @param aptaParaMenoresDeEdad 
     */
    public Pelicula(String titulo, String genero, String horario, String director, float duracion, String descripcionActores, String es3d, String aptaParaMenoresDeEdad)
    {
        this.titulo = titulo;
        this.genero = genero;
        this.horario = horario;
        this.director = director;
        this.duracion = duracion;
        this.descripcionActores = descripcionActores;
        this.es3d = es3d;
        this.aptaParaMenoresDeEdad = aptaParaMenoresDeEdad;
    }
    
    /**
     * Setter de la variable genero
     * @param genero 
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * Setter de la variable titulo
     * @param titulo 
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Setter de la variable horario
     * @param horario 
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * Setter de la variable director
     * @param director 
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * Setter de la variable duracion
     * @param duracion 
     */
    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    /**
     * Setter de la variable descripacionActores
     * @param descripcionActores 
     */
    public void setDescripcionActores(String descripcionActores) {
        this.descripcionActores = descripcionActores;
    }

    /**
     * Setter de la variable es3d
     * @param es3d 
     */
    public void setEs3d(String es3d) {
        this.es3d = es3d;
    }

    /**
     * Setter de la variable aptaParaMenoresDeEdad
     * @param aptaParaMenoresDeEdad 
     */
    public void setAptaParaMenoresDeEdad(String aptaParaMenoresDeEdad) {
        this.aptaParaMenoresDeEdad = aptaParaMenoresDeEdad;
    }

    /**
     * Getter de variable genero
     * @return String 
     */
    public String getGenero() 
    {
        return genero;
    }

    /**
     * Getter de variable titulo
     * @return String
     */
    public String getTitulo() 
    {
        return titulo;
    }

    /**
     * Getter de variable horario
     * @return String horario
     */
    public String getHorario()
    {
        return horario;
    }

    /**
     * Getter de variable director 
     * @return String 
     */
    public String getDirector() 
    {
        return director;
    }

    /**
     * Getter de variable duracion
     * @return String 
     */
    public float getDuracion() {
        return duracion;
    }

    /**
     * Getter de variable descripcionActores
     * @return String  
     */
    public String getDescripcionActores() 
    {
        return descripcionActores;
    }

    /**
     * Getter de variable es3d
     * @return String 
     */
    public String getEs3d() 
    {
        return es3d;
    }

    /**
     * Getter de variable aptaParaMenoresDeEdad
     * @return String 
     */
    public String getAptaParaMenoresDeEdad() 
    {
        return aptaParaMenoresDeEdad;
    }
    
    /**
     * Método estático que guarda las películas en el archivo peliculas.txt 
     * en la variable peliculas.
     */
    public static void listarPeliculas()
    {
        peliculas = new ArrayList<>();
        String peliculasString = ManejarTexto.ManejoTexto.obtenerPeliculas(ManejarTexto.ObtenerPath.obtenerPath("peliculas.txt")).toLowerCase();
        String[] pelis = peliculasString.split(("\n"));
        for (String pe:pelis)
        {
            String[] atributos = pe.split(",");
            
            String titulo = atributos[0].trim();
            String genero = atributos[1].trim();
            String horario = atributos[2];
            String director = atributos[3].trim();
            float duracion = Float.parseFloat(atributos[4]);
            String descripcionActores = atributos[5].trim();
            String es3d = atributos[6].trim();
            String aptaParaMenoresDeEdad = atributos[7].trim();
            
            Pelicula pelicula = new Pelicula(titulo,genero,horario,director,duracion,descripcionActores,es3d,aptaParaMenoresDeEdad);
            peliculas.add(pelicula);
        }   
    }
    
    /**
     * Getter de variable peliculas
     * @return ArrayList<Pelicula> 
     */
    
    public static ArrayList<Pelicula> mostrarPeliculas()
    {
        return peliculas;
    }
    
    /**
     * Método estático que muestra películas por género
     * @param op 
     */
    public static void mostrarPorGenero(String op)
    {
        ArrayList<ArrayList<Object>> auxiliar = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String opPelicula = new String("");
        Pelicula.listarPeliculas();
        String genero = "";
        if (op.equals("1"))
            genero = "ciencia ficcion";
        else if (op.equals("2"))
            genero = "drama";
        else if (op.equals("3"))
            genero = "terror";
        else if (op.equals("4"))
            genero = "thriller";
        else if (op.equals("5"))
            genero = "accion";
        else if (op.equals("6"))
            genero = "comedia";
        else if (op.equals("7"))
            genero = "romantica";
        else if (op.equals("8"))
            genero = "aventura";
        else if (op.equals("9"))
            genero = "animada";
        int contador = 1;
        
        for (Pelicula pelicula:Pelicula.mostrarPeliculas())
        {
            ArrayList<Object> contenedor = new ArrayList<>();
            if (pelicula.getGenero().equals(genero))
            {
                contenedor.add(contador);
                contenedor.add(pelicula);
                auxiliar.add(contenedor);
                System.out.println(contador+". "+pelicula.getTitulo());
                contador+=1;
            }
        }
        System.out.print("Que película desea ver: ");
        opPelicula = sc.nextLine();
        for (ArrayList<Object> pelicula:auxiliar)
        {
            if ((pelicula.get(0).toString()).equals(opPelicula))
                peliculaElejida = ((Pelicula) pelicula.get(1)); //downcasting
        } 
        
    }
    
    /**
     * Método estático que muestra películas por horario
     * @param hora
     * @return ArrayList<String>
     */
    public ArrayList<String> mostrarPorHora(String hora)
    {
        return null;
    }
    
    /**
     * Método estático que muestra películas por nombre
     * @param nombre
     * @return ArrayList<String>
     */
    public ArrayList<String> mostrarPorNombre(String nombre)
    {
        return null;
    }
    
    /**
    * Método estático que pide al usuario seleccionar el horario de la película seleccionada
    */
    public static void setHorarioPeliculaElejida()
    {
        {
            horarioElejido = new String("");
            Scanner sc = new Scanner(System.in);
            int contador = 1;
            String opcion = "";
            if (Horario.getHorario(peliculaElejida)==null)
            {
                System.out.println("No hay horarios disponibles");
            }
            else
            {
                for (String ej:(Horario.getHorario(peliculaElejida)))
                {
                    
                    System.out.println(contador+". "+ej);
                    contador+=1;
                }
                System.out.print("Elija el horario deseado: ");
                opcion = sc.nextLine();
                horarioElejido = Horario.getHorario(peliculaElejida)[Integer.parseInt(opcion)-1];
            }
        }
    }
    
    public static String getHorarioPeliculaElejida()
    {
        return horarioElejido;
    }
    /**
     * Getter de variable peliculaElejida
     * @return Pelicula
     */ 
    public static Pelicula getPeliculaElejida()
    {
        return peliculaElejida;
    }
    
    /**
     * Setter de variable peliculaElejida
     * @param pelicula 
     */
    public static void setPeliculaElejida(Pelicula pelicula)
    {
        peliculaElejida = pelicula;
    }
    
    /**
     * Sobreescritura de método toString
     * @return String 
     */
    @Override
    public String toString()
    {
        return ("Titulo: \t\t\t"+titulo+"\n"+
                "Genero: \t\t\t"+genero+"\n"+
                "Horario: \t\t\t"+horario+"\n"+
                "Director: \t\t\t"+director+"\n"+
                "Duracion: \t\t\t"+duracion+"\n"+
                "Descripcion de actores: \t"+descripcionActores+"\n"+
                "Disponible en 3D: \t\t"+es3d+"\n"+
                "Apta para menores de edad: \t"+aptaParaMenoresDeEdad+"\n");
    }
}
