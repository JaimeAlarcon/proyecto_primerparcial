/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;



/**
 *
 * @author David Vega
 * Clase que Establece los comportamientos de Documentales que hereda de Pelicula e implementa Reproducible
 */
public class Documentales extends Pelicula implements Reproducible
{
    private String tema;
    private boolean testReales;

    public Documentales()
    {
        this("",false);
    }
    /**
     * Constructor Sobrecargado 
     * @param tema
     * @param testReales 
     */
    public Documentales(String tema, boolean testReales)
    {
        this.tema = tema;
        this.testReales = testReales;
    }
    /**
     * Setter de Tema
     * @param tema 
     */
    public void setTema(String tema) {
        this.tema = tema;
    }
    /**
     * Setter de el atributo booleano testreales
     * @param testReales 
     */
    public void setTestReales(boolean testReales) {
        this.testReales = testReales;
    }
    /**
     * Getter de Tema
     * @return String que tiene como valor el atributo Tema 
     */
    public String getTema() {
        return tema;
    }
    /**
     * Getter de testReales 
     * @return El valor que tenga esa variable de instancia 
     */
    public boolean isTestReales() {
        return testReales;
    }    
    /**
     * Sobrescritura del metodo reproducir de la Interfaz reproducible
     * @return mensaje reproduciendo
     */
    @Override
    public String reproducir()
    {
        return "Reproduciendi";
    }
    /**
     * Sobrescritura del metodo pausar de la Interfaz reproducible
     * @return mensaje reproduciendo
     */
    @Override
    public String pausar()
    {
        return "Pausado";
    }
}
