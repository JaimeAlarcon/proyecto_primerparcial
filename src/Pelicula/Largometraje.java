/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;



/**
 *Clase que definie atributos y comportamientos de un largometraje que extiende de Pelicula e implementa de reproducible
 * @author Jaime Alarcon
 */
public class Largometraje extends Pelicula implements Reproducible
{
    private String url;
    private String sinopsis;

    public Largometraje()
    {
        this(new String(""), new String(""));
    }
    /**
     * Constructor Sobrecargado 
     * @param url
     * @param sinopsis 
     */
    public Largometraje(String url, String sinopsis)
    {
        this.url = url;
        this.sinopsis = sinopsis;
    }
    /**
     * Setter deURL
     * @param url 
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**+
     * Setter de Sinopsis
     * @param sinopsis 
     */
    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }
    /**
     * Getter deURL
     * @return 
     */
    public String getUrl() {
        return url;
    }
/**
 * Getter de Sinopsis0
 * @return 
 */
    public String getSinopsis() {
        return sinopsis;
    }  
    /**
     * Sobrescritura del metodo reproducir de la Interfaz reproducible
     * @return mensaje reproduciendo
     */
    @Override
    public String reproducir()
    {
        return "Reproduciendo";
    }
    /**
        * Sobrescritura del metodo pausar de la Interfaz reproducible
     * @return mensaje reproduciendo
     */
    @Override
    public String pausar()
    {
        return "Pausado";
    }
}
