/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pelicula;



/**
 *Interfaz Reproducible que define reproducir y pausar
 * @author David Vega
 */
public interface Reproducible 
{
    public String reproducir();
    
    public String pausar();
}
